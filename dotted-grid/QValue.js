
class QValue {


    constructor(model) {
        this.model = model;
    }

    getQValue(s, a, s1, r, dead) {
      let qValue = -1
      if (!dead) {
        let td = this.getTemporalDifference(s, a, s1, r);
        qValue = this.getQ(s, a) + (LEARNING_RATE * td)
      }
      return qValue;
    }
  
    // Eq. reward + (gmma/discount factor * max[A]Q(s1, a1)-Q(s, a))
    getTemporalDifference(s, a, s1, r) {
      let s1p = this.getQ(s1);
      let proximalFutureState = s1p[amax(s1p)]
      let preQV = this.getQ(s, a)
      return r + (GAMMA * (proximalFutureState - preQV))
    }

    getQ(s, a) {
        let qValues = this.model.predict(tf.tensor([s])).dataSync()
        if (typeof(a) == 'number') {
            return qValues[a]
        }
        return qValues;
    }


}
function drawDots() {
    let cols = H/scl;
    let rows = W/scl
    let grid = [];

    for (let i =0;i < cols;i++) {
        for (let k =0;k < rows;k++) {
            let x = i*scl
            let y = k*scl
            render4DotsInACell(x, y)
        }
    }
}

function render2DotsInACell(x, y, wallSide, nn, vertical = false) {

    let s = new Snake;
    let state = []
    s.x = x;
    s.y = y;
    if (wallSide == 'left') {
        state = [s.getState()[0]]
    } else if (wallSide == 'top') {
        state = [s.getState()[1]];
    } else if (wallSide == 'right') {
        state = [s.getState()[2]];
    } else if (wallSide == 'bottom') {
        state = [s.getState()[3]];
    }


    let QValues = this.getCellQValue(x, y, state, nn)
    noStroke()
    let colors = []
    if (QValues[0] > 0.9) {
        colors[0] = [255, 0, 0]
    } else {
        colors[0] = [0, 255, 0]
    }
    if (QValues[1] > 0.9) {
        colors[1] = [255, 0, 0]
    } else {
        colors[1] = [0, 255, 0]
    }

    let pos1, pos2;
    if (vertical) {
        pos1 = createVector(x+3, y+(scl/2))
        pos2 = createVector(x+(scl)-4, y+(scl/2))
    } else {
        pos1 = createVector(x+(scl/2), y+5)
        pos2 = createVector(x+(scl/2), y+scl-4)
    }

    fill(colors[0][0], colors[0][1], colors[0][2])
    ellipse(pos1.x, pos1.y, 8, 8) 

    fill(colors[1][0], colors[1][1], colors[1][2])
    ellipse(pos2.x, pos2.y, 8, 8) 
}

function getCellQValue(x, y, state, nn) {
    let s = new Snake;
    s.x = x;
    s.y = y;
    let QValue = nn.model.predict(tf.tensor([state])).dataSync()
    return QValue;
}


function render4DotsInACell(x, y) {
    let space = 0
    let QValues = this.getCellQValue(x, y)
    noStroke()
    let colors = []

    if (QValues[MOVE_UP] > 0) {
        colors[MOVE_UP] = [0, 255, 0]
    } else {
        colors[MOVE_UP] = [255, 0, 0]
    }

    if (QValues[MOVE_RIGHT] > 0) {
        colors[MOVE_RIGHT] = [0, 255, 0]
    } else {
        colors[MOVE_RIGHT] = [255, 0, 0]
    }

    if (QValues[MOVE_LEFT] > 0) {
        colors[MOVE_LEFT] = [0, 255, 0]
    } else {
        colors[MOVE_LEFT] = [255, 0, 0]
    }

    if (QValues[MOVE_DOWN] > 0) {
        colors[MOVE_DOWN] = [0, 255, 0]
    } else {
        colors[MOVE_DOWN] = [255, 0, 0]
    }

    fill(colors[MOVE_UP][0], colors[MOVE_UP][1], colors[MOVE_UP][2])
    ellipse(x+(scl/2), y + 5, 2, 2) // upside dot

    fill(colors[MOVE_RIGHT][0], colors[MOVE_RIGHT][1], colors[MOVE_RIGHT][2])
    ellipse(x+(scl)-4, y+(scl/2), 2, 2) // right side dot

    fill(colors[MOVE_DOWN][0], colors[MOVE_DOWN][1], colors[MOVE_DOWN][2])
    ellipse(x+(scl/2), y+(scl)-4, 2, 2) // bottom dot

    fill(colors[MOVE_LEFT][0], colors[MOVE_LEFT][1], colors[MOVE_LEFT][2])
    ellipse(x+4, y+(scl/2), 2, 2) // left dot
}

var previousClickedPlace = []
var clicked= false
function mouseClicked() {
    console.log(mouseX, mouseY)
    var clicked= true

    let cols = H/scl;
    let rows = W/scl
    let grid = [];

    for (let i =0;i < cols;i++) {
        for (let k =0;k < rows;k++) {
            let x = i*scl
            let y = k*scl
            if (mouseX > x && mouseX < x+scl && mouseY < y+scl && mouseY > y) {
                previousClickedPlace = [x, y]
                console.log(getCellQValue(x, y))
            }
        }
    }

}

function clickedHighligther() {
    fill(255, 0, 0, 100)
    rect(previousClickedPlace[0], previousClickedPlace[1], scl, scl)
    if (clicked) {
        setTimeout(() => {
            previousClickedPlace = []
            clicked= false
        }, 3000)
    }
}
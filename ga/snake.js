// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/AaGK-fj-BAM

class Snake {

  constructor() {
    this.color = [floor(random(255)), floor(random(255)),floor(random(255)),]
    let food = new Food(color);
    food.pickLocation()
    this.x = W/2;
    this.y = H/2;
    this.xspeed = 1;
    this.yspeed = 0;
    this.total = 1;
    this.tail = [];
    this.food = food;
    this.dead = false
    this.fitness = 0
    this.history = []

    this.brain = new NeuralNet(24,H_NODES,4,LAYERS)
  }

  eat() {
    let pos =this.food.food
    var d = dist(this.x, this.y, pos.x, pos.y);
    if (d < 1) {
      this.food.pickLocation()
      this.total++;
      return true;
    } else {
      return false;
    }
  }

  dir(x, y) {
    this.xspeed = x;
    this.yspeed = y;
    this.history.push({x: x, y: y})
  }

  death() {
    for (var i = 0; i < this.tail.length; i++) {
      var pos = this.tail[i];
      var d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        this.dead = true;
        // console.log('starting over');
        // this.total = 0;
        // this.tail = [];
      }
    }
  }

  bodyCollide(pos) {
    for ( let i = 0; i<this.tail.length-1;i++) {
      if (pos.x == this.tail[i].x && pos.y == this.tail[i].y) {
        return true;
      }
    }
    return false;
  }

  wallCollide(pos) {
    return pos.x < 0 || pos.y < 0 || pos.x > W - scl|| pos.y > H -scl;
  }

  update() {
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total >= 1) {
      this.tail[this.total - 1] = createVector(this.x, this.y);
    }

    this.x = this.x + this.xspeed * scl;
    this.y = this.y + this.yspeed * scl;

    this.x = constrain(this.x, 0, width - scl);
    this.y = constrain(this.y, 0, height - scl);
  }

  show() {
    if (this.dead) return;
    this.food.show()
    fill(this.color[0], this.color[1], this.color[2]);
    for (var i = 0; i < this.tail.length; i++) {
      rect(this.tail[i].x, this.tail[i].y, scl, scl);
    }
    rect(this.x, this.y, scl, scl);
  }

  getState() {
    let directions = [
      [0, -1],
      [0, 1],
      [1, 1],
      [-1, -1],
      [-1, 0],
      [1, 0],
      [1, -1],
      [-1, 1]
    ]
    let state = []

    for( let i =0; i < directions.length;i++) { 
      let temp = this.lookInDirection(directions[i]);
      state = state.concat(temp)
    }

    return state;
  }

  lookInDirection(direction) {
    let head = createVector(this.x, this.y);
    let pos = head.add(direction)
    let foodFound = false;
    let bodyFound = false;
    let state = [0,0,0]
    let distance = 1;
    pos.add(direction)
    while(!this.wallCollide(pos)) {
      if (!foodFound && this.foodCollide(pos)) {
        foodFound = true
        state[0] = 1;
      }
      if (!bodyFound && this.bodyCollide(pos)) {
        bodyFound = true
        state[1] = 1;
      }

      if(vision) {
          stroke(0,255,0);
          point(pos.x+scl/2,pos.y+scl/2);
          if(foodFound) {
            noStroke();
            fill(255,255,51);
            ellipseMode(CENTER);
            ellipse(pos.x,pos.y,5,5); 
          }
          if(bodyFound) {
            noStroke();
            fill(102,0,102);
            ellipseMode(CENTER);
            ellipse(pos.x,pos.y,5,5); 
          }
      }
      distance++;
      pos.add(direction)
    }

    state[2] = 1/distance;
    return state;
  }

  foodCollide(snake) {
    let pos =this.food.food
    var d = dist(snake.x, snake.y, pos.x, pos.y);
    return d < 1
  }

  takeAction(state) {
      let decision = this.brain.output(state);
      let maxIndex = 0;
      let max = 0;
      for(let i = 0; i < decision.length; i++) {
        if(decision[i] > max) {
          max = decision[i];
          maxIndex = i;
        }
      }
      
      switch(maxIndex) {
         case 0:
           this.moveUp();
           break;
         case 1:
           this.moveDown();
           break;
         case 2:
           this.moveLeft();
           break;
         case 3: 
           this.moveRight();
           break;
      }
  }

  moveUp() {
    if (this.yspeed != -1) {
      this.dir(0, 1)
      // this.xspeed = 0;
      // this.yspeed = 1;
    }
  }

  moveDown() {
    if (this.yspeed != 1) {
      this.dir(0, -1)
      // this.xspeed = 0;
      // this.yspeed = -1;
    }
  }

  moveLeft() {
    if (this.xspeed != 1) {
      this.dir(-1, 0)
      // this.xspeed = -1;
      // this.yspeed = 0;
    }
  }

  moveRight() {
    if (this.xspeed != -1) {
      this.dir(1, 0)
      // this.xspeed = 1;
      // this.yspeed= 0
    }
  }

  cloneForReplay() {  //clone a version of the snake that will be used for a replay
    let clone = new Snake(new Food);
    clone.brain = brain.clone();
    return clone;
 }
  
 clone() {  //clone the snake
  let clone = new Snake();
  clone.brain = this.brain.clone();
  return clone;
}

crossover(parent) {  //crossover the snake with another snake
  let child = new Snake();
  child.brain = this.brain.crossover(parent.brain);
  return child;
}

mutate() {  //mutate the snakes brain
  this.brain.mutate(MUTATION_RATE); 
}

}

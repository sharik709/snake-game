var scl = 20;
var population;

const W = 400;
const H = 400

let vision = false

const POP_SIZE = 10;

const REPLAY = true;

const FOOD_EAT_SCORE = 1
const DIE_SCORE = -1

const H_NODES = 16
const LAYERS = 2

const MUTATION_RATE = 0.5



function setup() {
  createCanvas(W, H);
  frameRate(10);
  population = new Population(POP_SIZE)
}

function draw() {
  background(51);


  population.handleFrame()

}
































function keyPressed() {
  if (keyCode == UP_ARROW) {
    takeAction(0, 1)
  } else if (keyCode == DOWN_ARROW) {
    takeAction(0, -1)
  } else if (keyCode == LEFT_ARROW) {
    takeAction(-1, 0)
  }else if(keyCode == RIGHT_ARROW) {
    takeAction(1, 0)
  }
}

function takeAction(x, y) {
  for (let i =0;i < population.pop.length;i++) {
    population.pop[i].dir(x, y)
  }
}

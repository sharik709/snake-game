// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/AaGK-fj-BAM

class Snake {

  constructor(foodLocation, takenActions) {
    this.colors = [floor(random(255)), floor(random(255)), floor(random(255))]
    let food = new Food(this.colors);
    food.pickLocation()
    this.replay = false;
    this.x = W / 2;
    this.y = H / 2;
    this.xspeed = 0;
    this.yspeed = 0;
    this.total = 1;
    this.tail = [];
    this.food = food;
    this.dead = false
    this.fitness = 0
    this.q_table = {}
    this.hide = false;
    this.score = 0;
    this.steps_without_food = 0;
  }

  eat() {
    let pos = this.food.food
    var d = dist(this.x, this.y, pos.x, pos.y);
    if (d < 1) {
      this.food.pickLocation()
      this.total++;
      this.score++;
      return true;
    } else {
      return false;
    }
  }

  dir(x, y) {
    this.xspeed = x;
    this.yspeed = y;
  }

  death() {
    for (var i = 0; i < this.tail.length; i++) {
      var pos = this.tail[i];
      var d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        this.dead = true;
        // console.log('starting over');
        // this.total = 0;
        // this.tail = [];
      }
    }
  }

  bodyCollide(pos) {
    for (let i = 0; i < this.tail.length - 1; i++) {
      if (pos.x == this.tail[i].x && pos.y == this.tail[i].y) {
        return true;
      }
    }
    return false;
  }

  wallCollide(pos) {
    return (pos.x > W - scl || pos.x < 0 || pos.y > H - scl || pos.y < 0)
  }

  update() {
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total >= 1) {
      this.tail[this.total - 1] = createVector(this.x, this.y);
    }

    this.x = this.x + this.xspeed * scl;
    this.y = this.y + this.yspeed * scl;

    this.x = constrain(this.x, 0, width - scl);
    this.y = constrain(this.y, 0, height - scl);
  }

  show() {
    if (this.dead) return;
    this.food.show()
    fill(this.colors[0], this.colors[1], this.colors[2]);
    for (var i = 0; i < this.tail.length; i++) {
      rect(this.tail[i].x, this.tail[i].y, scl, scl);
    }
    rect(this.x, this.y, scl, scl);
  }

  getState() {
    let state = []
    let temp = []

    temp = this.lookInDirection([0, -1])
    state[0] = temp[0]
    state[1] = temp[1]

    temp = this.lookInDirection([0, 1])
    state[2] = temp[0]
    state[3] = temp[1]

    temp = this.lookInDirection([-1, 0])
    state[4] = temp[0]
    state[5] = temp[1]

    temp = this.lookInDirection([1, 0])
    state[6] = temp[0]
    state[7] = temp[1]

    temp = this.lookInDirection([1, 1])
    state[8] = temp[0]
    state[9] = temp[1]

    temp = this.lookInDirection([-1, -1])
    state[10] = temp[0]
    state[11] = temp[1]

    temp = this.lookInDirection([1, -1])
    state[12] = temp[0]
    state[13] = temp[1]

    temp = this.lookInDirection([-1, 1])
    state[14] = temp[0]
    state[15] = temp[1]

    let head = createVector(this.x, this.y);
    let pos;

    pos = head.copy().add(0, -1)
    if (this.wallCollide(pos)) {
      state[16] = 0;
    } else {
      state[16] = 1
    }

    pos = head.copy().add(0, 1)
    if (this.wallCollide(pos)) {
      state[17] = 0;
    } else {
      state[17] = 1
    }

    pos = head.copy().add(-1, 0)
    if (this.wallCollide(pos)) {
      state[18] = 0;
    } else {
      state[18] = 1
    }

    pos = head.copy().add(1, 0)
    if (this.wallCollide(pos)) {
      state[19] = 0;
    } else {
      state[19] = 1
    }

    // wall check 
    // 16 - up
    // 17 - down
    // 18 - left
    // 19 - right
    return state;
  }

  lookInDirection(direction) {
    let head = createVector(this.x, this.y);
    let pos = head.add(direction)
    let foodFound = false;
    let bodyFound = false;
    let state = [0, 0]
    pos.add(direction)
    while (!this.wallCollide(pos)) {
      if (!foodFound && this.foodCollide(pos)) {
        foodFound = true
        state[0] = 1;
      }

      if (vision) {
        stroke(0, 255, 0);
        point(pos.x + scl / 2, pos.y + scl / 2);
        if (foodFound) {
          noStroke();
          fill(255, 255, 51);
          ellipseMode(CENTER);
          ellipse(pos.x, pos.y, 5, 5);
        }
        if (bodyFound) {
          noStroke();
          fill(102, 0, 102);
          ellipseMode(CENTER);
          ellipse(pos.x, pos.y, 5, 5);
        }
      }
      pos.add(direction)
    }
    return state;
  }

  foodCollide(snake) {
    let pos = this.food.food
    var d = dist(snake.x, snake.y, pos.x, pos.y);
    return d < 1
  }

  takeAction(a) {
    if (this.steps_without_food > 500) {
      this.dead = true;
      this.steps_without_food = 0
      return;
    }
    this.steps_without_food++;
    switch (a) {
      case 0:
        this.moveDown();
        break;
      case 1:
        this.moveUp();
        break;
      case 2:
        this.moveLeft();
        break;
      case 3:
        this.moveRight();
        break;
    }
  }

  moveUp() {
    if (this.yspeed != -1) {
      this.dir(0, 1)
      // this.xspeed = 0;
      // this.yspeed = 1;
    }
  }

  moveDown() {
    if (this.yspeed != 1) {
      this.dir(0, -1)
      // this.xspeed = 0;
      // this.yspeed = -1;
    }
  }

  moveLeft() {
    if (this.xspeed != 1) {
      this.dir(-1, 0)
      // this.xspeed = -1;
      // this.yspeed = 0;
    }
  }

  moveRight() {
    if (this.xspeed != -1) {
      this.dir(1, 0)
      // this.xspeed = 1;
      // this.yspeed= 0
    }
  }

  cloneForReplay() {  //clone a version of the snake that will be used for a replay
    let locationList = this.food.locationList;
    let clone = new Snake(locationList, this.takenActions);
    clone.brain = brain.clone();
    return clone;
  }

  clone() {  //clone the snake
    let clone = new Snake();
    clone.brain = this.brain.clone();
    return clone;
  }

}

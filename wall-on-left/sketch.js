var scl = 20;
const W = 600;
const H = 600;

var snake;
var leftWall, topWall;
var showBrain = true;
var LEARNING_RATE  = 0.1;

const FPS = 10;

const inputNodes    = 1;
const outputNodes   = 2;
const hiddenNodes   = 2;

const MOVE_UP = 0;
const MOVE_DOWN = 1;
const MOVE_LEFT = 2;
const MOVE_RIGHT = 3;


var pause       = false;
var step        = false;
var userControl = true;
var check       = true;
var pauseBtn, stepBtn, resumeBtn;
var leftWallModule, rightWallModule, topWallModule, BottomWallModule;

function setup() {
    createCanvas(W, H);
    frameRate(FPS)
    leftWall = new NeuralNet(inputNodes, hiddenNodes, outputNodes)

    snake = new Snake(leftWall);
    takeAction(2)

    this.setupButtons()
}   

function setupButtons() {
    pauseBtn = createButton('Pause')
    pauseBtn.position(700, 10)
    pauseBtn.mousePressed(pauseGame)

    stepBtn = createButton('Step ++')
    stepBtn.position(700, 30);
    stepBtn.mousePressed(stepGame)

    resumeBtn = createButton('Resume')
    resumeBtn.position(700, 60);
    resumeBtn.mousePressed(resumeGame)


    
}

function draw() {
    background(51)
    snake.show()


    let s = snake.getState();
    let predict = leftWall.model.predict(tf.tensor([s])).dataSync()
    renderDebugDots([
        [0, 0],
        [0, 20],
        [0, 40],
        [0, 60],
        [0, 80],
        [0, 100],
        [0, 120],
        [0, 140],
        [0, 160],
        [0, 180],
        [0, 200],
        [0, 220],
        [0, 240],
        [0, 260],
        [0, 280],
        [0, 300],
        [0, 320],
        [0, 300],
        [0, 320],
        [0, 340],
        [0, 360],
        [0, 380],
        [0, 400],
        [30, 300],
        [60, 300],
        [80, 300],
        [100, 300],
        [120, 300],
        [140, 300],
        [160, 300],
        [180, 300],
        [200, 300],
        [220, 300]
    ]);
    if (showBrain) {
        leftWall.render(s, predict)
    }
    if (!pause || step) {

        if (userControl == false) {
            let a = floor(random(3))
            takeAction(a)
        }

        if (predict[0] > 0.9 && snake.xspeed == -1) {
            snake.xspeed = 0;
            snake.yspeed = -1;
        }

        snake.update();
        snake.death()
        snake.eat()

        if (s[0] == 1 && snake.xspeed == -1) snake.memory.add(s, [1, 0], snake.dead)
        if (s[0] == 0 && snake.xspeed == -1) snake.memory.add(s, [0, 1], snake.dead)

        let samples = snake.memory.sample();
        if (samples.length > 0) {
            train_short_memory(samples)
        }
        if (snake.dead) {
            snake = new Snake
            takeAction(2)
        }
        step = false;
    }
    // drawDots()
    clickedHighligther()
}

function renderDebugDots(cords) {
    for (let i =0;i < cords.length;i++) {
        render2DotsInACell(cords[i][0], cords[i][1]);
    }
}

async function keyPressed() {
    if (keyCode == UP_ARROW) {
        takeAction(1)
    } else if (keyCode == DOWN_ARROW) {
        takeAction(0)
    } else if (keyCode == LEFT_ARROW) {
        takeAction(2)
    } else if (keyCode == RIGHT_ARROW) {
        takeAction(3)
    }
}

async function train_left_short_memory(samples) {
    for (let i =0;i < samples.length;i++) {
        await leftWall.model.fit(tf.tensor([samples[i].inputs]), tf.tensor([samples[i].targets]), {
            callbacks: {
                onEpochEnd: async (epoch, log) => {
                    console.log(`Epoch ${epoch}: loss = ${log.loss}`);
                }
            }
        })
    }
}

function resumeGame() {
    pause = false;
}

function pauseGame() {
    pause = true;
}

function stepGame() {
    step = true;
}

function takeAction(a) {

    if (snake.steps_without_food > 100) {
        snake.dead = true;
        snake.steps_without_food = 0
        return;
    }
    snake.steps_without_food++;
    switch (a) {
        case 0:
            snake.moveUp();
            break;
        case 1:
            snake.moveDown();
            break;
        case 2:
            snake.moveLeft()
            break;
        case 3:
            snake.moveRight()
            break;
    }
}
class NeuralNet {
    
    constructor(input, hidden, output, hiddenLayers) {
      this.iNodes = input;
      this.hNodes = hidden;
      this.oNodes = output;
      this.hLayers = hiddenLayers;
      
      this.weights = []
      this.weights[0] = new Matrix(this.hNodes, this.iNodes+1);
      for(let i=1; i<this.hLayers; i++) {
         this.weights[i] = new Matrix(this.hNodes,this.hNodes+1); 
      }
      this.weights[this.weights.length-1] = new Matrix(this.oNodes,this.hNodes+1);
      for(let i= 0; i < this.weights.length-1;i++) {
         this.weights[i].randomize(); 
      }
    }
    
     mutate(mr) {
       for(let i =0;i< this.weights.length-1;i++) {
          this.weights[i].mutate(mr); 
       }
    }
    
    output(inputsArr) {
       let inputs = this.weights[0].singleColumnMatrixFromArray(inputsArr);
       
       let curr_bias = inputs.addBias();
       
       for(let i=0; i<this.hLayers; i++) {
          let hidden_ip = this.weights[i].dot(curr_bias); 
          let hidden_op = hidden_ip.activate();
          curr_bias = hidden_op.addBias();
       }
       
       let output_ip = this.weights[this.weights.length-1].dot(curr_bias);
       let output = output_ip.activate();
       
       return output.toArray();
    }
    
    crossover(partner) {
       let child = new NeuralNet(this.iNodes,this.hNodes,this.oNodes,this.hLayers);
       for(let i=0; i<this.weights.length-1; i++) {
          child.weights[i] = this.weights[i].crossover(partner.weights[i]);
       }
       return child;
    }
    
    clone() {
       let clone = new NeuralNet(this.iNodes,this.hNodes,this.oNodes,this.hLayers);
       for(let i=0; i<this.weights.length-1; i++) {
          clone.weights[i] = this.weights[i].clone(); 
       }
       
       return clone;
    }
    
    load(weight) {
        for(let i=0; i<weights.length-1; i++) {
           this.weights[i] = weight[i]; 
        }
    }
    
    pull() {
       return this.weights.clone();
    }
    
  }
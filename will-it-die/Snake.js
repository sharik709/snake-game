// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/AaGK-fj-BAM

class Snake {

  constructor(foodLocation, takenActions) {
    this.colors = [floor(random(255)), floor(random(255)), floor(random(255))]
    this.x = W / 2;
    this.y = H / 2;
    this.xspeed = 0;
    this.yspeed = 0;
    this.total = 1;
    this.tail = [];
    this.dead = false
    this.fitness = 0
    this.q_table = {}
    this.hide = false;
    this.score = 0;
    this.steps_without_food = 0;
  }

  eat() {
    let pos = this.food.food
    var d = dist(this.x, this.y, pos.x, pos.y);
    if (d < 1) {
      this.food.pickLocation()
      this.total++;
      return true;
    } else {
      return false;
    }
  }

  dir(x, y) {
    this.xspeed = x;
    this.yspeed = y;
  }

  death() {
    for (var i = 0; i < this.tail.length; i++) {
      var pos = this.tail[i];
      var d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        this.dead = true;
        // console.log('starting over');
        // this.total = 0;
        // this.tail = [];
      }
    }
  }

  bodyCollide(pos) {
    for (let i = 0; i < this.tail.length - 1; i++) {
      if (pos.x == this.tail[i].x && pos.y == this.tail[i].y) {
        return true;
      }
    }
    return false;
  }

  wallCollide(pos) {
    return (pos.x > W - scl || pos.x < 0 || pos.y > H - scl || pos.y < 0)
  }

  update() {
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total >= 1) {
      this.tail[this.total - 1] = createVector(this.x, this.y);
    }

    this.x = this.x + this.xspeed * scl;
    this.y = this.y + this.yspeed * scl;

    this.x = constrain(this.x, 0, width - scl);
    this.y = constrain(this.y, 0, height - scl);
  }

  show() {
    if (this.dead) return;
    fill(this.colors[0], this.colors[1], this.colors[2]);
    for (var i = 0; i < this.tail.length; i++) {
      rect(this.tail[i].x, this.tail[i].y, scl, scl);
    }
    rect(this.x, this.y, scl, scl);
  }

  getState() {
    let head  = createVector(this.x, this.y);
    let state = []
    let pos;

    pos = head.copy().add(0, -1)
    if (this.wallCollide(pos)) {
      state[0] = 1;
    } else {
      state[0] = 0
    }

    pos = head.copy().add(0, 1)
    if (this.wallCollide(pos)) {
      state[1] = 1;
    } else {
      state[1] = 0
    }

    pos = head.copy().add(-1, 0)
    if (this.wallCollide(pos)) {
      state[2] = 1;
    } else {
      state[2] = 0
    }

    pos = head.copy().add(1, 0)
    if (this.wallCollide(pos)) {
      state[3] = 1;
    } else {
      state[3] = 0
    }

    // wall check 
    // 16 - up
    // 17 - down
    // 18 - left
    // 19 - right
    return state;
  }

  lookInDirection(direction) {
    let head = createVector(this.x, this.y);
    let pos = head.add(direction)
    let foodFound = false;
    let bodyFound = false;
    let state = [0, 0]
    pos.add(direction)
    while (!this.wallCollide(pos)) {
      if (!foodFound && this.foodCollide(pos)) {
        foodFound = true
        state[0] = 1;
      }

      if (vision) {
        stroke(0, 255, 0);
        point(pos.x + scl / 2, pos.y + scl / 2);
        if (foodFound) {
          noStroke();
          fill(255, 255, 51);
          ellipseMode(CENTER);
          ellipse(pos.x, pos.y, 5, 5);
        }
        if (bodyFound) {
          noStroke();
          fill(102, 0, 102);
          ellipseMode(CENTER);
          ellipse(pos.x, pos.y, 5, 5);
        }
      }
      pos.add(direction)
    }
    return state;
  }

  foodCollide(snake) {
    let pos = this.food.food
    var d = dist(snake.x, snake.y, pos.x, pos.y);
    return d < 1
  }

  takeOppositeAction(a) {
    if (this.steps_without_food > 100) {
      this.dead = true;
      this.steps_without_food = 0
      return;
    }
    this.score++;
    this.steps_without_food++;
    switch (a) {
      case 0:
        this.moveLeft();
        break;
      case 1:
        this.moveRight();
        break;
      case 2:
        this.moveUp()
        break;
      case 3:
        this.moveDown();
        break;
      case 4:
        console.log('not dying anywhere')
    }
  }

  moveUp() {
    if (this.yspeed != -1) {
      this.dir(0, 1)
      // this.xspeed = 0;
      // this.yspeed = 1;
    }
  }

  moveDown() {
    if (this.yspeed != 1) {
      this.dir(0, -1)
      // this.xspeed = 0;
      // this.yspeed = -1;
    }
  }

  moveLeft() {
    if (this.xspeed != 1) {
      this.dir(-1, 0)
      // this.xspeed = -1;
      // this.yspeed = 0;
    }
  }

  moveRight() {
    if (this.xspeed != -1) {
      this.dir(1, 0)
      // this.xspeed = 1;
      // this.yspeed= 0
    }
  }

  cloneForReplay() {  //clone a version of the snake that will be used for a replay
    let clone = new Snake()
    clone.brain = brain.clone();
    return clone;
  }

  clone() {  //clone the snake
    let clone = new Snake();
    clone.brain = this.brain.clone();
    return clone;
  }

}

class NeuralNetwork {


    constructor(inputs, hidden, outputs, layers, learning_rate) {
        this.inputNodes     = inputs;
        this.hiddenNodes    = hidden;
        this.outputNodes    = outputs;
        this.layers         = layers;
        this.learning_rate  = learning_rate;
        this.model          = tf.sequential();
        tf.setBackend('cpu')
        this.generateNetwork()
    }

    generateNetwork() {
        this.model.add(tf.layers.dense({
            inputShape: this.inputNodes,
            units: this.hiddenNodes,
            activation: 'relu'
        }))

        for (let i =0;i < this.layers-2;i++) {
            this.model.add(tf.layers.dense({
                units: this.hiddenNodes,
                activation: 'relu'
            }))
        }

        this.model.add(tf.layers.dense({
            units: this.outputNodes,
            activation: 'relu'
        }))


        // [-1, -0.3, 0.24, 0.25]
        // [0, 0, 0.4, 0.6]

        this.model.compile({
            loss: 'meanSquaredError',
            optimizer: tf.train.adam(this.learning_rate)
        })
    }

    async train(inputs, targets, epoch = 10) {
        await this.model.fit(tf.tensor([inputs]), tf.tensor[targets], epoch)
    }


}
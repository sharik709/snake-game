class BW {

    constructor() {
        this.nn = new NeuralNet(1, 2, 2);
        let drawBrain = new DrawBrain(this.nn.model, {
            startingX: 100,
            startingY: 370,
            layerGap: 120,
            nodeGap: 70,
            inputLabels: ['Is wall at bottom?'],
            outputLabels: ['I think ill die moving down', 'I think ill not die moving down']
        })
        this.nn.drawBrain = drawBrain
        this.memory = new Memory;
    }

    predict(s) {
        let predict = this.nn.predict(s);
        return predict
    }

    async train() {
        let samples = this.memory.sample();
        if (samples.length == 0) return;
        for( let i =0;i < samples.length;i++) {
            await this.nn.train(samples[i].inputs, samples[i].targets)
        }
    }

    renderBrain(state, predict) {
        this.nn.render(state, predict);
    }

    clone() {
        let BWM = new BW;
        BWM.nn = this.nn;
        return BWM;
    }



}
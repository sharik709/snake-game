function drawDots() {
    let cols = H/scl;
    let rows = W/scl
    let grid = [];

    for (let i =0;i < cols;i++) {
        for (let k =0;k < rows;k++) {
            let x = i*scl
            let y = k*scl
            render4DotsInACell(x, y)
        }
    }
}

function render2DotsInACell(x, y) {
    let space = 0
    let QValues = this.getCellQValue(x, y)
    noStroke()
    let colors = []
    if (QValues[0] > 0.9) {
        colors[0] = [255, 0, 0]
    } else {
        colors[0] = [0, 255, 0]
    }
    if (QValues[1] > 0.9) {
        colors[1] = [255, 0, 0]
    } else {
        colors[1] = [0, 255, 0]
    }

    fill(colors[0][0], colors[0][1], colors[0][2])
    ellipse(x+3, y +(scl/2), 8, 8) 

    fill(colors[1][0], colors[1][1], colors[1][2])
    ellipse(x+(scl)-4, y+(scl/2), 8, 8) 
}



function render4DotsInACell(x, y) {
    let space = 0
    let QValues = this.getCellQValue(x, y)
    noStroke()
    let colors = []

    if (QValues[MOVE_UP] > 0) {
        colors[MOVE_UP] = [0, 255, 0]
    } else {
        colors[MOVE_UP] = [255, 0, 0]
    }

    if (QValues[MOVE_RIGHT] > 0) {
        colors[MOVE_RIGHT] = [0, 255, 0]
    } else {
        colors[MOVE_RIGHT] = [255, 0, 0]
    }

    if (QValues[MOVE_LEFT] > 0) {
        colors[MOVE_LEFT] = [0, 255, 0]
    } else {
        colors[MOVE_LEFT] = [255, 0, 0]
    }

    if (QValues[MOVE_DOWN] > 0) {
        colors[MOVE_DOWN] = [0, 255, 0]
    } else {
        colors[MOVE_DOWN] = [255, 0, 0]
    }

    fill(colors[MOVE_UP][0], colors[MOVE_UP][1], colors[MOVE_UP][2])
    ellipse(x+(scl/2), y + 5, 2, 2) // upside dot

    fill(colors[MOVE_RIGHT][0], colors[MOVE_RIGHT][1], colors[MOVE_RIGHT][2])
    ellipse(x+(scl)-4, y+(scl/2), 2, 2) // right side dot

    fill(colors[MOVE_DOWN][0], colors[MOVE_DOWN][1], colors[MOVE_DOWN][2])
    ellipse(x+(scl/2), y+(scl)-4, 2, 2) // bottom dot

    fill(colors[MOVE_LEFT][0], colors[MOVE_LEFT][1], colors[MOVE_LEFT][2])
    ellipse(x+4, y+(scl/2), 2, 2) // left dot
}

function getCellQValue(x, y, direction) {
    let s = new Snake;
    s.x = x;
    s.y = y;
    let state = s.getState();
    let QValue = leftWall.model.predict(tf.tensor([state])).dataSync()
    return QValue;
}

var previousClickedPlace = []
var clicked= false
function mouseClicked() {
    console.log(mouseX, mouseY)
    var clicked= true

    let cols = H/scl;
    let rows = W/scl
    let grid = [];

    for (let i =0;i < cols;i++) {
        for (let k =0;k < rows;k++) {
            let x = i*scl
            let y = k*scl
            if (mouseX > x && mouseX < x+scl && mouseY < y+scl && mouseY > y) {
                previousClickedPlace = [x, y]
                console.log(getCellQValue(x, y))
            }
        }
    }

}

function clickedHighligther() {
    fill(255, 0, 0, 100)
    rect(previousClickedPlace[0], previousClickedPlace[1], scl, scl)
    if (clicked) {
        setTimeout(() => {
            previousClickedPlace = []
            clicked= false
        }, 3000)
    }
}
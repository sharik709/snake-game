class DQN {

    constructor(memorySize) {
        this.replayMemory = new ReplayMemory(memorySize)
    }

}

class ReplayMemory {
    constructor(memorySize) {
        this.memorySize = memorySize;
        this.memory = []
    }

    append(experience) {
        if (this.memory.length == this.memorySize) {
            this.memory.shift();
        }
        this.memory.push(experience)
    }

    clear() {
        this.memory = []
    }

    sample(batchSize = 50) {
        let samples = [];
        for (let i = 0;i < batchSize;i++) {
            samples[i] = this.memory[floor(random(this.memory.length-1))];
        }
        return samples;
    }
}
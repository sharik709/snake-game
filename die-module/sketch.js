const W = 600;
const H = 600;
const FR = 10;
const vision = false;
const GAMMA = 0.9
const LEARNING_RATE = 0.1

const BACKGROUND_COLOR = 51;

var snake;
var scl = 20;
var nn;


let trainAgent = [
    {
        inputs: [0, 1, 1, 1],
        targets: [1, 0, 0, 0]
    },
    {
        inputs: [1, 0, 1, 1],
        targets: [0, 1, 0, 0]
    },
    {
        inputs: [1, 1, 0, 1],
        targets: [0, 0, 1, 0]
    },
    {
        inputs: [1, 1, 1, 0],
        targets: [0, 0, 0, 1]
    },
]



const inputs        = 4;
const hidden        = 8;
const outputs       = 4;
const layers        = 2;
const learning_rate = 0.1

function setup() {
    createCanvas(W, H);
    frameRate(FR);

    snake   = new Snake
    nn      = new NeuralNetwork(inputs, hidden, outputs,layers, learning_rate)

    for(let i =0;i < trainAgent.length;i++) {
        nn.train(trainAgent[i].inputs, trainAgent[i].targets)
    }

}

function draw() {
    background(BACKGROUND_COLOR);
    snake.update();
    snake.show();
    snake.eat()
    snake.death()
    if (snake.dead) {
        snake = new Snake()
    }

    let state = snake.getState();

    let s = state.splice(state.length-4)
    let predict = nn.model.predict(tf.tensor2d([s])).dataSync()
    console.log(s)
    if (predict[0] == 0.25 && predict[1] == 0.25 && predict[2] == 0.25 && predict[3] == 0.25) {
        return;
    }
    console.log(predict)
    let a = amax(predict)

    if (a == 0) {
        a = 3;
        console.log('AI: will die up')
    }

    if (a == 1) {
        a = 2;
        console.log('AI: will die down')
    }

    if (a == 2) {
        a = 1;
        console.log('AI: will die left')
    }

    if (a == 3) {
        a = 2;
        console.log('AI: will die right')
    }

    takeAction(a)
    // let reward = getReward(s, a);
    // let s1 = snake.getState().splice(state.length-4)
    // let qValue = snake.getQWithoutUpdate(s, a, s1, reward)
    // let target = [0, 0, 0, 0]
    // target[a] = qValue
    // console.log(reward)
    // nn.model.fit(tf.tensor2d([s]), tf.tensor2d([target]), 10)
    // console.log(state)
}

function train() {
    
}

function getReward(s, a) {
    console.log(s)
    if (s[0] == 1) {
        if (a==3) {
            return 1;
        } else {
            return -1;
        }
    }

    if (s[1] == 1) {
        if (a==2) {
            return 1;
        } else {
            return -1;
        }
    }

    if (s[2] == 1) {
        if (a==1) {
            return 1;
        } else {
            return -1;
        }
    }

    if (s[3] == 1) {
        if (a==2) {
            return 1;
        } else {
            return -1;
        }
    }

    return 0;
}

function keyPressed() {
    switch (keyCode) {
        case UP_ARROW:
            snake.dir(0, -1)
            break;
        case DOWN_ARROW:
            snake.dir(0, 1)
            break;
        case LEFT_ARROW:
            snake.dir(-1, 0)
            break;
        case RIGHT_ARROW:
            snake.dir(1, 0)
            break;
    }
}

function amax(x) {
    let xi = -1;
    let maxV = 0;
    for (let i =0;i < x.length;i++) {
        if (x[i] > maxV) {
            maxV = x[i]
            xi = i;
        }
    }
    return xi
}


function takeAction(a) {
    switch(a) {
        case 0: 
            snake.dir(0, 1)
            break;
        case 1:
            snake.dir(0, -1)
            break
        case 2:
            snake.dir(-1, 0);
            break;
        case 3:
            snake.dir(1, 0);
            break;
    }
}
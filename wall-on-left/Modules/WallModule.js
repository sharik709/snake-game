class WallModule {

    constructor() {
        this.setupNework();
    }

    setupNetwork() {
        this.model = tf.sequential()
        this.model.add(tf.layers.dense({
            units: 2,
            inputShape: 1,
            activation: 'softmax'
        }))
        this.model.compile({
            optimizer: tf.train.Adam(0.01),
            loss: 'meanSquaredError'
        })
    }

    predict(s) {
        let predict = this.model.predict(tf.tensor([s]));
        return predict
    }

    train(input, target) {
        this.model.fit(tf.tensor([input]), tf.tensor([target]))
    }



}
class LTC {



    constructor(nn, config = {}) {
        if (nn) {
            this.nn = nn
        }else {
            this.nn = new NeuralNet(4, 2, 3, 'softmax')
        }
        this.config = config;
        let drawBrain = new DrawBrain(this.nn.model, {
            startingX: 100,
            startingY: 100,
            layerGap: 120,
            nodeGap: 70,
            inputLabels: ['will i die moving left', 'will i die moving top', 'am i moving left', 'am I moving up'],
            outputLabels: ['i only feel safe going right', 'i only feel safe going down', 'I feel safe going up or left']
        })
        this.drawBrain = drawBrain
        this.badMemory = new Memory;
        this.goodMemory = new Memory;
    }

    whereDoIfeelSafe(state) {
        let predict = this.predict(state);

        let feelSafeOnRightIndex = 0;
        let feelSafeDownIndex    = 1;
        let safeUpAndLeftIndex    = 2; // not gonna do anything. As this output index means there is nothing to worry about

        let whatDoIFeel = this.maximumIndex(predict);
        let safeAction  = undefined;

        if (feelSafeDownIndex == whatDoIFeel) {
            safeAction = 1;
        }
        if (feelSafeOnRightIndex == whatDoIFeel) {
            safeAction = 3;
        }

        this.postPrediction(state, predict)

        return safeAction;
    }

    postPrediction(state, predict) {
        if (this.config.renderBrain) {
            this.renderBrain(state, predict)
        }
    }

    predict(s) {
        return this.nn.predict(s);
    }

    async train() {
        let samples = [];


        for (let i =0;i < this.badMemory.inputs.length;i++) {
            samples.push({inputs: this.badMemory.inputs[i], outputs: this.badMemory.targets[i]})
        }

        for (let i =0; i < this.badMemory.inputs.length;i++) {
            samples.push({inputs:this.goodMemory.inputs[i], outputs: this.goodMemory.targets[i]})
        }
        for (let i =0;i < samples.length;i++) {
            await this.nn.train(samples[i].inputs, samples[i].outputs, {
                epochs: 10
            });
        }

    }


    renderBrain(state, predict) {
        this.drawBrain.render(state, predict)
    }

    clone() {
       return new LTC(this.nn, this.config);
    }

    maximumIndex(a) {
        let maxI = -1;
        let maxValue = 0;

        for (let i =0; i < a.length;i++) {
            if (maxValue < a[i]) {
                maxI = i;
                maxValue = a[i]
            }
        }
        return maxI;
    }

}
// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/AaGK-fj-BAM

class Snake {

  constructor() {
    let food = new Food();
    food.pickLocation()
    this.x = W/2;
    this.y = H/2;
    this.xspeed = 1;
    this.yspeed = 0;
    this.total = 1;
    this.tail = [];
    this.food = food;
    this.dead = false
    this.fitness = 0
    this.q_table = {}
    this.hide = false;
  }

  eat() {
    let pos =this.food.food
    var d = dist(this.x, this.y, pos.x, pos.y);
    if (d < 1) {
      this.food.pickLocation()
      this.total++;
      return true;
    } else {
      return false;
    }
  }

  dir(x, y) {
    this.xspeed = x;
    this.yspeed = y;
  }

  death() {
    for (var i = 0; i < this.tail.length; i++) {
      var pos = this.tail[i];
      var d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        this.dead = true;
        // console.log('starting over');
        // this.total = 0;
        // this.tail = [];
      }
    }
  }

  bodyCollide(pos) {
    for ( let i = 0; i<this.tail.length-1;i++) {
      if (pos.x == this.tail[i].x && pos.y == this.tail[i].y) {
        return true;
      }
    }
    return false;
  }

  wallCollide(pos) {
    return (pos.x > W - scl || pos.x < 0 || pos.y > H -scl || pos.y < 0 )
  }

  update() {
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total >= 1) {
      this.tail[this.total - 1] = createVector(this.x, this.y);
    }

    this.x = this.x + this.xspeed * scl;
    this.y = this.y + this.yspeed * scl;

    this.x = constrain(this.x, 0, width - scl);
    this.y = constrain(this.y, 0, height - scl);
  }

  show() {
    if (this.dead) return;
    this.food.show()
    fill(255);
    for (var i = 0; i < this.tail.length; i++) {
      rect(this.tail[i].x, this.tail[i].y, scl, scl);
    }
    rect(this.x, this.y, scl, scl);
  }

  getState() {
    let directions = [
       [0, -1], //0,1, 2
      [0, 1], // 3, 4,5
      [-1, 0],
      [1, 0],
      [1, 1],
      [-1, -1],
      [1, -1],
      [-1, 1]
    ]
    let state = []

    for( let i =0; i < directions.length;i++) { 
      let temp = this.lookInDirection(directions[i]);
      state = state.concat(temp)
    }

    let head = createVector(this.x, this.y);
    // wall check 
    for ( let i =0; i < 4; i++) {
      let pos = head.copy().add(directions[i][0], directions[i][1])
      if (this.wallCollide(pos)) {
        state[state.length] = 1;
      } else {
        state[state.length] = 0
      }
    }


    return state;
  }

  lookInDirection(direction) {
    let head = createVector(this.x, this.y);
    let pos = head.add(direction)
    let foodFound = false;
    let bodyFound = false;
    let state = [0,0,0]
    pos.add(direction)
    while(!this.wallCollide(pos)) {
      if (!foodFound && this.foodCollide(pos)) {
        foodFound = true
        state[0] = 1;
      }

      if(vision) {
          stroke(0,255,0);
          point(pos.x+scl/2,pos.y+scl/2);
          if(foodFound) {
            noStroke();
            fill(255,255,51);
            ellipseMode(CENTER);
            ellipse(pos.x,pos.y,5,5); 
          }
          if(bodyFound) {
            noStroke();
            fill(102,0,102);
            ellipseMode(CENTER);
            ellipse(pos.x,pos.y,5,5); 
          }
      }
      pos.add(direction)
    }
    return state;
  }

  getQ(s, a) {
    let q = this.q_table[s.join()]
    if (!q || !q[a]){
      return 0;
    }
    return q[a]
  }

  setQ(s, a, q) {
    if (!this.q_table[s.join()]) {
        this.q_table[s.join()] = {}
    }
    this.q_table[s.join()][a] = q
    return this.q_table
  }

  // Eq. Q(s, a) + LEARNING Rate * TD(s, a)
  updateQTable(s, a, s1, r) {
    let qValue = -100000
    if (!this.dead) {
      let td = this.getTemporalDifference(s, a, s1, r);
      qValue = this.getQ(s, a) + (LEARNING_RATE * td)
    }
    this.setQ(s, a, qValue)
  }

  // Eq. reward + (gmma/discount factor * max[A]Q(s1, a1)-Q(s, a))
  getTemporalDifference(s, a, s1, r) {
    let proximalFutureState = Math.max(this.getQ(s1, 0), this.getQ(s1, 1), this.getQ(s1, 2), this.getQ(s1, 3));
    let preQV = this.getQ(s, a)
    return r + (GAMMA * (proximalFutureState - preQV))
  }

  getAction(s) {
    let QValues = [this.getQ(s, 0), this.getQ(s, 1), this.getQ(s, 2), this.getQ(s, 3)];
    let maxI = -1;
    let maxV = 0;
    for (let i =0;i < QValues.length;i++) {
      if (maxV < QValues[i]){
        maxV = QValues[i]
        maxI = i
      }
    }
    if  (maxI == -1) {
      maxI = floor(random(0, 3))
    }
    return maxI;
  }

  foodCollide(snake) {
    let pos =this.food.food
    var d = dist(snake.x, snake.y, pos.x, pos.y);
    return d < 1
  }

  takeAction(a) {
      switch(a) {
         case 0:
           this.moveDown();
           break;
         case 1:
           this.moveUp();
           break;
         case 2:
           this.moveLeft();
           break;
         case 3: 
           this.moveRight();
           break;
      }
  }

  getReward(s, a) {
    let reward = 0;

    if (s[6] == 1 && a == 2) {
      reward = 100;
    } else if (s[0] == 1 && a == 0) {
      reward = 100;
    } else if (s[9] == 1 && a == 3) {
      reward = 100;
    } else if(s[3] == 1 && a == 1) {
      reward = 100
    }

    if (s[27] == 1 && a== 3) {
      reward = -1000;
    } else if(s[25] ==1 && a == 1) {
      reward = -1000;
    } else if (s[24] == 1 && a == 0) {
      reward = -1000;
    } else if(s[26] == 1 && a == 2) {
      reward = -1000;
    }

    if (this.eat()) {
      reward = 20000
    }

    if (this.dead) {
      reward = -1000
    }

    return reward;
  }

  moveUp() {
    if (this.yspeed != -1) {
      this.dir(0, 1)
      // this.xspeed = 0;
      // this.yspeed = 1;
    }
  }

  moveDown() {
    if (this.yspeed != 1) {
      this.dir(0, -1)
      // this.xspeed = 0;
      // this.yspeed = -1;
    }
  }

  moveLeft() {
    if (this.xspeed != 1) {
      this.dir(-1, 0)
      // this.xspeed = -1;
      // this.yspeed = 0;
    }
  }

  moveRight() {
    if (this.xspeed != -1) {
      this.dir(1, 0)
      // this.xspeed = 1;
      // this.yspeed= 0
    }
  }

  cloneForReplay() {  //clone a version of the snake that will be used for a replay
    let clone = new Snake(new Food);
    clone.brain = brain.clone();
    return clone;
 }
  
 clone() {  //clone the snake
  let clone = new Snake();
  clone.brain = this.brain.clone();
  return clone;
}

crossover(parent) {  //crossover the snake with another snake
  let child = new Snake();
  child.brain = this.brain.crossover(parent.brain);
  return child;
}

mutate() {  //mutate the snakes brain
  this.brain.mutate(MUTATION_RATE); 
}

}

class RW {

    constructor(nn, config = {}) {
        if (nn) {
            this.nn = nn;
        } else {
            this.nn = new NeuralNet(1, 2, 2);
        }
        this.config = config;
        let drawBrain = new DrawBrain(this.nn.model, {
            startingX: 100,
            startingY: 260,
            layerGap: 120,
            nodeGap: 70,
            inputLabels: ['Is wall on right?'],
            outputLabels: ['I think ill die moving to right', 'I think ill not die moving right']
        })
        this.nn.drawBrain = drawBrain;
        this.memory = new Memory;
    }

    willIDieOnRight(state) {
        return this.predict(state)[0] > 0.9 ? 1 : 0;
    }

    predict(state) {
        let predict = this.nn.predict(state);
        this.postPrediction([state], predict)
        return predict
    }

    postPrediction(state, predict) {
        if (this.config.renderBrain) {
            this.renderBrain([state], predict)
        }
    }

    async train() {
        let samples = this.memory.sample();
        if (samples.length == 0) return;
        for( let i =0;i < samples.length;i++) {
            await this.nn.train(samples[i].inputs, samples[i].targets)
        }
    }

    renderBrain(state, predict) {
        this.nn.render(state, predict);
    }

    clone() {
        return new RW(this.nn, this.config);
    }


}

var scl = 20;
const W = 600;
const H = 600;

var snake;
var nn;

const FPS = 10;

const inputNodes    = 1;
const outputNodes   = 2;

var pause = false;
var step = false;

var pauseBtn, stepBtn, resumeBtn;
var ACTION = 0

function setup() {
    createCanvas(W, H);
    snake = new Snake;
    snake.takeAction(ACTION)
    frameRate(FPS)
    nn = new NeuralNet(1, 2)


    pauseBtn = createButton('Pause')
    pauseBtn.position(10, 10)
    pauseBtn.mousePressed(pauseGame)

    stepBtn = createButton('Step ++')
    stepBtn.position(10, 30);
    stepBtn.mousePressed(stepGame)

    resumeBtn = createButton('Resume')
    resumeBtn.position(10, 60);
    resumeBtn.mousePressed(resumeGame)

}

function resumeGame() {
    pause = false;
}

function pauseGame() {
    pause = true;
}

function stepGame() {
    step= true;
}

function draw() {
    background(51)
    
    let a = 3;

    snake.show()
    if (!pause || step) {
        snake.eat()

        snake.update();
        step = false;
        let s = snake.getState()
        let predict = nn.predict(s)
        snake.takeAction(ACTION)
        snake.death()
        if (predict[0] > predict[1]) {
            console.log('You will dieeeeee')
        }
        if (snake.dead) {
            snake = new Snake
            snake.takeAction(ACTION)
        }

        if (!snake.dead) {
            train_short_memory(s, predict);
        }

    }

}

async function train_short_memory(s, predict)
{
    if (s[0] == 1) {
        console.log('---------')
        console.log(s, predict)
        predict[0] = 1;
        nn.model.fit(tf.tensor([s]), tf.tensor([predict]))
        return;
    }
    predict[0] = 0;
    await nn.model.fit(tf.tensor([s]), tf.tensor([predict]))
}
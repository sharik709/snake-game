class RW {

    constructor(nn) {
        if (nn) {
            this.nn = nn;
        } else {
            this.nn = new NeuralNet(1, 2, 2);
        }
        let drawBrain = new DrawBrain(this.nn.model, {
            startingX: 100,
            startingY: 260,
            layerGap: 120,
            nodeGap: 70,
            inputLabels: ['Is wall on right?'],
            outputLabels: ['I think ill die moving to right', 'I think ill not die moving right']
        })
        this.nn.drawBrain = drawBrain;
        this.memory = new Memory;
    }

    predict(s) {
        let predict = this.nn.predict(s);
        return predict
    }

    async train() {
        let samples = this.memory.sample();
        if (samples.length == 0) return;
        for( let i =0;i < samples.length;i++) {
            await this.nn.train(samples[i].inputs, samples[i].targets)
        }
    }

    renderBrain(state, predict) {
        this.nn.render(state, predict);
    }

    clone() {
        return new RW(this.nn);
    }


}
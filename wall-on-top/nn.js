class NeuralNet {

   constructor(inputs, outputs) {
      this.model = tf.sequential();
      this.model.add(tf.layers.dense({
         units: outputs,
         inputShape: inputs,
         activation: 'softmax'
      }))
      tf.setBackend('cpu')
      this.model.compile({
         loss: 'meanSquaredError',
         optimizer: tf.train.adam(0.1)
      })
   }

   predict(s) {
      return this.model.predict(tf.tensor([s])).dataSync();
   }
}
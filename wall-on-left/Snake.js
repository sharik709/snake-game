// Daniel Shiffman
// http://codingtra.in
// http://patreon.com/codingtrain
// Code for: https://youtu.be/AaGK-fj-BAM
var nnn;
class Snake {

  constructor(nn) {
    this.food = new Food;
    this.food.pickLocation()
    this.x = W / 2;
    this.y = H / 2;
    this.xspeed = 0;
    this.yspeed = 0;
    this.total = 1;
    this.tail = [];
    this.dead = false
    this.fitness = 0
    this.hide = false;
    this.score = 0;
    this.steps_without_food = 0;
    nnn = nn;
    this.memory = new Memory;
  }

  eat() {
    let pos = this.food.food
    var d = dist(this.x, this.y, pos.x, pos.y);
    if (d < 1) {
      this.food.pickLocation()
      this.total++;
      return true;
    } else {
      return false;
    }
  }

  dir(x, y) {
    this.xspeed = x;
    this.yspeed = y;
  }

  death() {
    for (var i = 0; i < this.tail.length; i++) {
      var pos = this.tail[i];
      var d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        this.dead = true;
      }
    }
  }

  bodyCollide(pos) {
    for (let i = 0; i < this.tail.length - 1; i++) {
      if (pos.x == this.tail[i].x && pos.y == this.tail[i].y) {
        return true;
      }
    }
    return false;
  }

  wallCollide(pos) {
    return (pos.x > W - scl || pos.x < 0 || pos.y > H - scl || pos.y < 0)
  }

  update() {
    for (let i = 0; i < this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i + 1];
    }
    if (this.total >= 1) {
      this.tail[this.total - 1] = createVector(this.x, this.y);
    }

    this.x = this.x + this.xspeed * scl;
    this.y = this.y + this.yspeed * scl;

    this.x = constrain(this.x, 0, width - scl);
    this.y = constrain(this.y, 0, height - scl);
  }

  show() {
    if (this.dead) return;
    fill(255);
    for (var i = 0; i < this.tail.length; i++) {
      rect(this.tail[i].x, this.tail[i].y, scl, scl);
    }
    rect(this.x, this.y, scl, scl);
  }

  getState() {
    let head  = createVector(this.x, this.y);
    let state = []
    let pos;

    pos = head.copy().add(-1, 0)
    if (this.wallCollide(pos)) {
      state[0] = 1;
    } else {
      state[0] = 0
    }
    return state;
  }

  foodCollide(snake) {
    let pos = this.food.food
    var d = dist(snake.x, snake.y, pos.x, pos.y);
    return d < 1
  }


  moveUp() {
    if (this.yspeed != -1) {
      this.dir(0, 1)
      // this.xspeed = 0;
      // this.yspeed = 1;
    }
  }

  moveDown() {
    if (this.yspeed != 1) {
      this.dir(0, -1)
      // this.xspeed = 0;
      // this.yspeed = -1;
    }
  }

  moveLeft() {
    if (this.xspeed != 1) {
      this.dir(-1, 0)
      // this.xspeed = -1;
      // this.yspeed = 0;
    }
  }

  moveRight() {
    if (this.xspeed != -1) {
      this.dir(1, 0)
      // this.xspeed = 1;
      // this.yspeed= 0
    }
  }

  cloneForReplay() {  //clone a version of the snake that will be used for a replay
    let clone = new Snake()
    clone.brain = brain.clone();
    return clone;
  }

  clone() {  //clone the snake
    let clone = new Snake();
    clone.brain = this.brain.clone();
    return clone;
  }

}

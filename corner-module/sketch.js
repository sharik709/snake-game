var scl = 20;
const W = 600;
const H = 600;

var snake;
var leftWall, topWall;
var showBrain = true;
var LEARNING_RATE  = 0.1;

const FPS = 10;

const inputNodes    = 1;
const outputNodes   = 2;
const hiddenNodes   = 2;

const MOVE_UP = 0;
const MOVE_DOWN = 1;
const MOVE_LEFT = 2;
const MOVE_RIGHT = 3;


var pause       = false;
var step        = false;
var userControl = true;
var check       = true;
var pauseBtn, stepBtn, resumeBtn;
var LWM, TWM, RWM;
var userActed = false;

function setup() {
    createCanvas(W, H);
    frameRate(FPS)
    LWM = new LW;
    TWM = new TW;
    RWM = new RW;
    BWM = new BW;
    LTCM = new LTC;


    snake = new Snake();
    takeAction(2)

    this.setupButtons()
}   

function setupButtons() {
    pauseBtn = createButton('Pause')
    pauseBtn.position(700, 10)
    pauseBtn.mousePressed(pauseGame)

    stepBtn = createButton('Step ++')
    stepBtn.position(700, 30);
    stepBtn.mousePressed(stepGame)

    resumeBtn = createButton('Resume')
    resumeBtn.position(700, 60);
    resumeBtn.mousePressed(resumeGame)
}

function draw() {
    background(51)
    snake.show()


    let s = snake.getState();

    let LWpredict = LWM.predict([s[0]]);
    let TWpredict = TWM.predict([s[1]]);
    let RWpredict = RWM.predict([s[2]]);
    let BWpredict = BWM.predict([s[3]]);

    // LWM.renderBrain([s[0]], LWpredict);
    // TWM.renderBrain([s[1]], TWpredict);
    // RWM.renderBrain([s[2]], RWpredict);
    // BWM.renderBrain([s[3]], BWpredict);

    let LTCState = [LWpredict[0] > 0.90 ? 1 : 0, TWpredict[0] > 0.90 ? 1: 0, snake.xspeed == -1 ? 1 : 0, snake.yspeed == -1 ? 1: 0]

    let ltcPredict = LTCM.predict(LTCState);
    LTCM.renderBrain(LTCState, ltcPredict)
    var decidedAction;

    renderDebugDots([[0, 220], [0, 240], [0, 260], [0, 280], [0, 300], [0, 320]],'left', LWM.nn, true)
    renderDebugDots([[220, 0], [240, 0], [260, 0], [280, 0], [300, 0], [320, 0]],'top', TWM.nn)
    renderDebugDots([[W-scl, 220], [W-scl, 240], [W-scl, 260], [W-scl, 280], [W-scl, 300], [W-scl, 320]],'right', RWM.nn, true)
    renderDebugDots([[220, H-scl], [240, H-scl], [260, H-scl], [280, H-scl], [300, H-scl], [320, H-scl]],'bottom', BWM.nn)

    if (!pause || step || userActed) {

        if (userControl == false) {
            let a = floor(random(3))
            takeAction(a)
        }

        if (isSnakeInCorner()) {

            if (s[0] == 1 && s[1] == 1) {
                if (ltcPredict[0] > 0.90 && snake.xspeed == -1) {
                    decidedAction = 1;
                } else if (ltcPredict[1] > 0.90 && snake.yspeed == -1) {
                    decidedAction = 3;
                } else {
                    decidedAction = floor(random(3))
                }
                
            }
            takeAction(decidedAction)
        } else {
            if (LWpredict[0] > 0.9 && snake.yspeed != 1) {
                snake.xspeed = 0;
                snake.yspeed = -1;
            }

            if (TWpredict[0] > 0.9 && snake.xspeed != -1) {
                snake.xspeed = 1;
                snake.yspeed = 0;
            }

            if (RWpredict[0] > 0.9 && snake.yspeed != -1) {
                snake.xspeed = 0;
                snake.yspeed = 1;
            }

            if (BWpredict[0] > 0.9 && snake.xspeed != 1) {
                snake.xspeed = -1;
                snake.yspeed = 0;
            }
        }
        snake.update();
        snake.death()


        if (snake.dead) {


            if (s[0] == 1 && snake.xspeed == -1) LWM.memory.add([s[0]], [1, 0])
            if (s[1] == 1 && snake.yspeed == -1) TWM.memory.add([s[1]], [1, 0])
            if (s[2] == 1 && snake.xspeed == 1) RWM.memory.add([s[2]], [1, 0])
            if (s[3] == 1 && snake.yspeed == 1) BWM.memory.add([s[3]], [1, 0])
            if (s[0] == 1 && s[1] == 1) {

                if (snake.yspeed == -1) {
                    ltcPredict = [1, 0, 0];
                } else if (snake.xspeed == -1) {
                    ltcPredict = [0, 1, 0]
                }

                LTCM.badMemory.add(LTCState, ltcPredict)
                LTCM.train()
            }



            LWM.train();
            TWM.train();
            RWM.train();
            BWM.train();



            snake = new Snake

            // following will create new instance of modules and assign there brain and update current nueral network
            LWM = LWM.clone();
            TWM = TWM.clone();
            RWM = RWM.clone();
            BWM = BWM.clone();
            LTCM = LTCM.clone();
            takeAction(2)
        } else {
            if (s[0] == 0 && snake.xspeed == -1) LWM.memory.add([s[0]], [0, 1])
            if (s[1] == 0 && snake.yspeed == -1) TWM.memory.add([s[1]], [0, 1])
            if (s[2] == 0 && snake.xspeed == 1) RWM.memory.add([s[2]], [0, 1])
            if (s[3] == 0 && snake.yspeed == 1) BWM.memory.add([s[3]], [0, 1])

            ltcPredict = [0, 0, 1];
            LTCM.goodMemory.add(LTCState, ltcPredict)
        }




        step = false;
        userActed = false
    }
}

function renderDebugDots(cords, state, nn, vertical = false) {
    for (let i =0;i < cords.length;i++) {
        render2DotsInACell(cords[i][0], cords[i][1], state, nn, vertical);
    }
}

async function keyPressed() {
    if (keyCode == UP_ARROW && snake.yspeed != 1) {
        userActed = true
        snake.xspeed = 0;
        snake.yspeed = -1;
    } else if (keyCode == DOWN_ARROW && snake.yspeed != -1) {
        userActed = true
        snake.xspeed = 0;
        snake.yspeed = 1;
    } else if (keyCode == LEFT_ARROW && snake.xspeed != 1) {
        userActed = true
        snake.xspeed = -1;
        snake.yspeed = 0;
    } else if (keyCode == RIGHT_ARROW && snake.xspeed != -1) {
        userActed = true
        snake.xspeed = 1;
        snake.yspeed = 0;
    }
}

function isSnakeInCorner() {
    let s = snake.getState();
    return (s[0] == 1 && s[1] == 1) || (s[1] == 1 && s[2] == 1) || (s[2] == 1 && s[3] == 1) || (s[3] == 1 && s[0] == 1)
}

function resumeGame() {
    pause = false;
}

function pauseGame() {
    pause = true;
}

function stepGame() {
    step = true;
}

function takeAction(a) {

    if (snake.steps_without_food > 100) {
        snake.dead = true;
        snake.steps_without_food = 0
        return;
    }
    snake.steps_without_food++;
    switch (a) {
        case 0:
            snake.moveUp();
            break;
        case 1:
            snake.moveDown();
            break;
        case 2:
            snake.moveLeft()
            break;
        case 3:
            snake.moveRight()
            break;
    }
}
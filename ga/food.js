class Food {

    constructor(color) {
        this.color = color;
    }

    pickLocation() {
        var cols = floor(width/scl);
        var rows = floor(height/scl);
        this.food = createVector(floor(random(cols)), floor(random(rows)));
        this.food.mult(scl);
    }

    show() {
        fill(this.color[0], this.color[1], this.color[2])
        rect(this.food.x, this.food.y, scl, scl)
    }
  
}
class LTC {



    constructor(nn) {
        if (nn) {
            this.nn = nn
        }else {
            this.nn = new NeuralNet(4, 2, 3, 'softmax')
        }
        let drawBrain = new DrawBrain(this.nn.model, {
            startingX: 100,
            startingY: 100,
            layerGap: 120,
            nodeGap: 70,
            inputLabels: ['will i die moving left', 'will i die moving top', 'am i moving left', 'am I moving up'],
            outputLabels: ['I can go down', 'I can go right', 'I am not in the left corner']
        })
        this.drawBrain = drawBrain
        this.badMemory = new Memory;
        this.goodMemory = new Memory;
    }



    predict(s) {
        return this.nn.predict(s);
    }

    async train() {
        let samples = [];


        for (let i =0;i < this.badMemory.inputs.length;i++) {
            samples.push({inputs: this.badMemory.inputs[i], outputs: this.badMemory.targets[i]})
        }

        for (let i =0; i < this.badMemory.inputs.length;i++) {
            samples.push({inputs:this.goodMemory.inputs[i], outputs: this.goodMemory.targets[i]})
        }

        for (let i =0;i < samples.length;i++) {
            await this.nn.train(samples[i].inputs, samples[i].outputs, {
                epochs: 10
            });
        }

    }


    renderBrain(state, predict) {
        this.drawBrain.render(state, predict)
    }

    clone() {
       return new LTC(this.nn);
    }


}
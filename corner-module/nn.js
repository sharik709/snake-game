class NeuralNet {

    constructor(inputs, hidden, outputs, activation = 'softmax' ) {
        this.inputs = inputs;
        this.outputs = outputs;
        this.hidden = hidden;

        this.model = tf.sequential();
        this.model.add(tf.layers.dense({
            units: this.outputs,
            inputShape: inputs,
            activation: activation
        }))
        tf.setBackend('cpu')
        this.model.compile({
            loss: 'meanSquaredError',
            optimizer: tf.train.adam(0.1)
        })

        this.drawBrain = new DrawBrain(this.model, {
            startingX: 100,
            startingY: 100,
            layerGap: 120,
            nodeGap: 70,
            inputLabels: ['Is wall on left?'],
            outputLabels: ['I think ill die moving on left', 'I think ill not die moving left']
        })
    }

    predict(s) {
        return this.model.predict(tf.tensor([s])).dataSync();
    }

    render(state, predict, config = {}) {
        return this.drawBrain.render(state, predict, config);
    }

    async train(input, target) {
        return await this.model.fit(tf.tensor([input]), tf.tensor([target]), {
                epochs: 10,
                callbacks: {
                    onEpochEnd: async (epoch, log) => {
                        // console.log(`Epoch ${epoch}: loss = ${log.loss}`);
                    }
                }
            });
        }

    }
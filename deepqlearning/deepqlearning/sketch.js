const scl = 20;
const W = 400;
const H = 400;

const inputNodes    = 1;
const hiddenNodes   = 1;
const outputNodes   = 1;
const layers        = 1;
const learning_rate = 0.1;
const FR            = 100;
const vision        = false;
const KILL_WITHOUT_FOOD_STEPS = 1000;

const BATCH_SIZE = 100;

const GAMMA = 0.9
const LEARNING_RATE = learning_rate

const MOVE_UP = 0;
const MOVE_DOWN = 1;
const MOVE_LEFT = 2;
const MOVE_RIGHT = 3;

var EXPLORATION = 0.99999999;
const EXPLORATION_DECAY  = 0.00002;
const MINIMUM_EXPLORATION_RATE = 0.5;

var snake;
var dqn;
var nn;
var kill_steps= 0; 
var score = 0;
var QClass;
var gen = 0;

var pause = false;
var act = false;

var pauseBtn;
var resumeBtn;
var stepBtn;

function setup() {
    createCanvas(W, H);
    frameRate(FR);
    snake   = new Snake;
    dqn     = new DQN(1000);
    nn      = new NeuralNetwork(inputNodes, hiddenNodes, outputNodes, layers, learning_rate)
    QClass  = new QValue(nn.model)

    pauseBtn = createButton('Pause Game')
    pauseBtn.position(700,10)
    pauseBtn.mousePressed(pauseGame)

    resumeBtn = createButton('Resume Game')
    resumeBtn.position(700,30)
    resumeBtn.mousePressed(resumeGame)

    stepBtn = createButton('Take Step')
    stepBtn.position(700,50)
    stepBtn.mousePressed(takeAStep)
}

function pauseGame() {
    pause = true;
}

function resumeGame() {
    pause = false;
}

function takeAStep() {
    act = true;
}

async function draw() {
    background(51);
    // displayText();

    if (pause && !act) {
        snake.show();
        return;
    }
    act = false;

    let s = snake.getState();
    let rnd = random()
    let a = 0;
    if (rnd < 0.20) {
        if (!EXPLORATION <= MINIMUM_EXPLORATION_RATE) {
            EXPLORATION -= EXPLORATION_DECAY
        }
        a = floor(random(3));
    } else {
        a = amax(nn.model.predict(tf.tensor([s])).dataSync())
    }

    if (!pause) {
        execAction(a);
        act = false;
    }

    snake.update()
    snake.show()

    let eatReward = false;
    if(snake.eat()) {
        score++;
        eatReward = true;
        kill_steps = 0;
    }

    snake.death()
    if (kill_steps == KILL_WITHOUT_FOOD_STEPS) {
        snake.dead = true;
        kill_steps = 0;
    }
    kill_steps++;   
    let s1  = snake.getState();
    let r   = snake.getReward(s, a, eatReward)
    eatReward = false;

    dqn.replayMemory.append({s: s, a: a,s1: s1, r: r, crash: snake.dead})
    await train_short_memory(s, a, s1, r)

    if (snake.dead) {
        trainBatch()
        snake = new Snake;
        gen++;
    }
}

async function train_short_memory(s, a, s1, r) {
    let qValue = QClass.getQValue(s, a, s1, r)
    let predict = nn.model.predict(tf.tensor([s])).dataSync()
    predict[a] = qValue;
    await nn.model.fit(tf.tensor([s]), tf.tensor([predict]), 10);
}


function amax(x) {
    let xi = -1;
    let maxV = 0;
    for (let i =0;i < x.length;i++) {
        if (x[i] > maxV) {
            maxV = x[i]
            xi = i;
        }
    }
    return xi
}

function execAction(a) {
    if (MOVE_UP == a && snake.yspeed != -1) {
        snake.dir(0, 1);
    } else if (MOVE_DOWN == a && snake.yspeed != 1) {
        snake.dir(0, -1)
    } else if (MOVE_LEFT == a && snake.xspeed != 1) {
        snake.dir(-1, 0)
    } else if (MOVE_RIGHT == a && snake.xspeed != -1) {
        snake.dir(1, 0)
    }
    return false
}

function isItReverseAction(a) {
    if (MOVE_UP == a && snake.yspeed == -1) {
        return true;
    } else if (MOVE_DOWN == a && snake.yspeed == 1) {
        return true;
    } else if (MOVE_LEFT == a && snake.xspeed == 1) {
        return true
    } else if (MOVE_RIGHT == a && snake.xspeed == -1) {
        return true;
    }
    return false
}

function getXYForAction(a) {
    if (MOVE_UP == a) {
        return [0, 1]
    } else if (MOVE_DOWN == a) {
        return [0, -1]
    } else if (MOVE_LEFT == a) {
        return [-1, 0]
    } else if (MOVE_RIGHT == a && snake.xspeed == -1) {
        return [1, 0]
    }
}

function getCurrenctAction() {
    if (snake.xspeed == 0 && snake.yspeed == 1) {
        return 0;
    } else if (snake.xspeed == 0 && snake.yspeed == -1) {
        return 1;
    } else if (snake.xspeed == -1 && snake.yspeed == 0) {
        return 2;
    } else if (snake.xspeed == 1 && snake.yspeed == 0) {
        return 3;
    }
}

function displayText() {
    text('Learning Rate:'+ LEARNING_RATE, 10, 20)
    text('Hidden Nodes:'+ hiddenNodes, 10, 40)
    text('Batch Size:'+ BATCH_SIZE, 10, 60)
    text('Score:'+ score, 10, 80)
    text('Gen:'+ gen, 10, 100)
    text('Exploration Rate:'+ EXPLORATION, 10, 120)
}

async function trainBatch() {
    let batchExperience = dqn.replayMemory.sample(BATCH_SIZE);
    for (let i =0;i < batchExperience.length;i++ ) {
        qValue = QClass.getQValue(batchExperience[i].s, batchExperience[i].a, batchExperience[i].s1, batchExperience[i].r, batchExperience[i].crash)
        let predict = nn.model.predict(tf.tensor([batchExperience[i].s])).dataSync()
        predict[batchExperience[i].a] = qValue;
        console.log(qValue, predict[batchExperience[i].a])
        await nn.model.fit(tf.tensor([batchExperience[i].s]), tf.tensor([predict]), {
            epochs: 10
        });
        console.log(qValue, predict[batchExperience[i].a])
        // dqn.replayMemory.clear()
    }
}
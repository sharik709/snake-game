class Food {


    constructor(colors) {
        this.colors = colors
    }

    pickLocation() {
        var cols = floor(width/scl);
        var rows = floor(height/scl);
        this.food = createVector(floor(random(cols)), floor(random(rows)));
        this.food.mult(scl);
    }

    show() {
        fill(this.colors[0], this.colors[1], this.colors[2])
        rect(this.food.x, this.food.y, scl, scl)
    }
  
}
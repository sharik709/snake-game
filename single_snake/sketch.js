var scl = 20;

const W = 400;
const H = 400;

const GAMMA = 0.9;
const LEARNING_RATE = 0.1

var EXPLORATION = 0.1
const EXPLORARATION_DECAY = 0.00001

const humanPlaying = false;


var FR = 100

let vision = false
var snake;

var pauseBtn;
var actBtn;
var pauseGame= false;
var act = false


function setup() {
  createCanvas(W, H);
  frameRate(FR);
  snake = new Snake();
  pauseBtn = createButton('Pause Game')
  pauseBtn.position(440, 100)
  pauseBtn.mousePressed(pauseTheGame)

  actBtn = createButton('Step')
  actBtn.position(440, 200)
  actBtn.mousePressed(stepGame)

}

function keyPressed() {
  switch (keyCode) {
    case UP_ARROW:
      snake.dir(0, -1)
      break;
    case DOWN_ARROW:
      snake.dir(0, 1)
      break;
    case LEFT_ARROW:
      snake.dir(-1, 0)
      break;
    case RIGHT_ARROW:
      snake.dir(1, 0)
      break;
  }
}

function pauseTheGame() {
  pauseGame = true
  console.log(pauseGame)
}

function stepGame() {
  act = true
}

function draw() {
  background(51);


  if (humanPlaying) {
    if (snake.dead) {
      snake = new Snake
    }
    snake.show()
    if (!pauseGame || act) {
      snake.update()
      if (snake.eat()) {
        console.log(snake.score+=10)
      }
      snake.death()
      act = false;
    }
  } else if (!snake.dead) {
    let state = snake.getState();
    let action = snake.getAction(state);
    let rand = random();
    if (rand < EXPLORATION) {
      action = floor(random(0, 3))
    }
    snake.takeAction(action)
    snake.update()
    snake.show()
    snake.death()
    let reward = snake.getReward(state, action);
    let newState = snake.getState();
    snake.updateQTable(state, action, newState, reward)
    act = false;
    EXPLORATION -= EXPLORARATION_DECAY
  } else {
    let s = snake;
    snake = new Snake;
    snake.q_table = s.q_table
  }
}
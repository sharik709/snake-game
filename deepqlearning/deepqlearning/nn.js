class NeuralNetwork {


    constructor(inputs, hidden, outputs, layers, learning_rate) {
        this.inputNodes     = inputs;
        this.hiddenNodes    = hidden;
        this.outputNodes    = outputs;
        this.layers         = layers;
        this.learning_rate  = learning_rate;
        this.model          = tf.sequential();
        tf.setBackend('cpu')
        this.generateNetwork()
        this.resetWeightsToZeros()
    }

    resetWeightsToZeros() {
        let layers = this.model.layers;
        for (let i =0;i < layers.length;i++) {
            let ow = layers[i].getWeights();
            let aw = []
            for (let k =0; k < ow.length;k++) {
                aw.push(tf.zeros(ow[k].shape))
            }
            this.model.layers[i].setWeights(aw)
        }
    }

    generateNetwork() {
        this.model.add(tf.layers.dense({
            inputShape: this.inputNodes,
            units: this.hiddenNodes,
            activation: 'relu'
        }))

        for (let i =0;i < this.layers-2;i++) {
            this.model.add(tf.layers.dense({
                units: this.hiddenNodes,
                activation: 'relu'
            }))
        }

        this.model.add(tf.layers.dense({
            units: this.outputNodes,
            activation: 'softmax'
        }))

        this.model.compile({
            loss: 'meanSquaredError',
            optimizer: tf.train.sgd(this.learning_rate)
        })
    }



}

class Memory {


    constructor(limit) {
        this.limit = limit;
        this.inputs = []
        this.targets= []
    }

    add(inputs, targets)  {
        this.inputs.push(inputs)
        this.targets.push(targets);
    }

    sample() {
        let samples = [];
        for (let i = 0; i < this.inputs.length;i++) {
            if(this.inputs[i][0] == 1) {
                samples.push({
                    inputs: this.inputs[i],
                    targets: this.targets[i]
                })
            }
        }

        let samplesLength = samples.length;
        for (let i = 0; i < samplesLength;i++) {
            if(this.inputs[i][0] == 0) {
                samples.push({
                    inputs: this.inputs[i],
                    targets: this.targets[i]
                })
            }
        }
        return samples;
    }

    clear() {
        this.memory = []
    }


}
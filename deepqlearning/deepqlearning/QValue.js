
class QValue {


    constructor(model) {
        this.model = model;
    }

    getQValue(s, a, s1, r, dead) {
      let qValue = -1
      if (!dead) {
        let td = this.getTemporalDifference(s, a, s1, r);
        let preQV = this.getQ(s, a)
        let error = this.getError(td, preQV)
        qValue = error
      }
      return qValue;
    }

    getError(val1, val2) {
      return val1 - val2
    }
  
    // Eq. reward + (gmma/discount factor * max[A]Q(s1, a1)-Q(s, a))
    getTemporalDifference(s, a, s1, r) {
      let s1p = this.getQ(s1);
      let proximalFutureState = s1p[amax(s1p)]
      return r + (GAMMA * proximalFutureState)
    }

    getQ(s, a) {
        let qValues = this.model.predict(tf.tensor([s])).dataSync()
        if (typeof(a) == 'number') {
            return qValues[a]
        }
        return qValues;
    }


}
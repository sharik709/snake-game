
var scl = 20;
const W = 600;
const H = 600;

var snakes = [];

var neat;
var vision = false;

const POP_SIZE = 10;

const inputNodes    = 4;
const outputNodes   = 5;
const MUTATION_RATE = 0.1

var bestPlayer;

var replay = false;

let config = {
	model: [
		{nodeCount: inputNodes, type: "input"},
		{nodeCount: outputNodes, type: "output", activationfunc: activation.SOFTMAX}
	],
	mutationRate: MUTATION_RATE,
	crossoverMethod: crossover.RANDOM,
	mutationMethod: mutate.RANDOM,
	populationSize: POP_SIZE
};

function setup() {
    createCanvas(W, H);
    for (let i =0;i<POP_SIZE;i++) {
        snakes[i] = new Snake;
    }

    neat = new NEAT(config)
}

function draw() {
    background(51)
    for (let i = 0; i< snakes.length;i++) {
        neat.setInputs(snakes[i].getState(), i)
    }
    neat.feedForward()
    
    let actions = neat.getDesicions()
    for (let i = 0;i < snakes.length;i++) {
        snakes[i].takeOppositeAction(actions[i])
        snakes[i].update();
        snakes[i].show()
        snakes[i].death()
    }

    if (isAllDead()) {
        for (let i = 0; i< snakes.length;i++) {
            neat.setFitness(snakes[i].score, i)
            snakes[i] = new Snake
        }
        neat.doGen()
    }
}

function isAllDead() {
    for (let i =0;i < snakes.length;i++) {
        if(!snakes[i].dead) {
            return false;
        }
    }
    return true;
}

function getBestPlayer() {
    let maxScore = -1;
    let bestSnake = null
    for (let i = 0;i < snakes.length;i++) {
        if (maxScore < snakes[i].score) {
            maxScore = snakes[i].score;
            bestSnake = snakes[i]
            neat.export(i)
        }
    }
    return bestSnake.cloneForReply()
}
const scl = 20;
const W = 400;
const H = 400;

const inputNodes    = 28;
const hiddenNodes   = 18;
const outputNodes   = 4;
const layers        = 2;
const learning_rate = 0.01;
const FR            = 100;
const vision        = false;
const KILL_WITHOUT_FOOD_STEPS = 1000;
var EXPLORATION = 0.10;
const EXPLORATION_DECAY  = 0.995;
const BATCH_SIZE = 100;

const GAMMA = 0.9
const LEARNING_RATE = 0.5

const MOVE_UP = 0;
const MOVE_DOWN = 1;
const MOVE_LEFT = 2;
const MOVE_RIGHT = 3;

var snake;
var dqn;
var nn;
var kill_steps= 0; 
var score = 0;

function setup() {
    createCanvas(W, H);
    frameRate(FR);
    snake = new Snake;
    dqn = new DQN(1000);
    nn = new NeuralNetwork(inputNodes, hiddenNodes, outputNodes, layers, learning_rate)
}

async function draw() {
    background(51);
    text('Learning Rate:'+ LEARNING_RATE, 10, 20)
    text('Hidden Nodes:'+ hiddenNodes, 10, 40)
    text('Batch Size:'+ BATCH_SIZE, 10, 60)
    text('Score:'+ score, 10, 90)
    let s = snake.getState();
    let rnd = random()
    let a = 0;
    // if (rnd < EXPLORATION) {
    //     a = floor(random(3));
    // } else {
        a = amax(nn.model.predict(tf.tensor([s])).dataSync())
    // }
    execAction(a);
    snake.update()
    snake.show()
    snake.death()

    if(snake.eat()) {
        score++;
        kill_steps = 0;
    }
    if (kill_steps == KILL_WITHOUT_FOOD_STEPS) {
        snake.dead = true;
        kill_steps = 0;
    }
    kill_steps++;   
    let s1  = snake.getState();
    let r   = snake.getReward(s, a)

    dqn.replayMemory.append({s: s, a: a,s1: s1, r: r, crash: snake.dead})

    await train_short_memory(s, a, s1, r)

    if( snake.dead ) {
        let batchExperience = dqn.replayMemory.sample(BATCH_SIZE);
        for (let i =0;i < batchExperience.length;i++ ) {
            let qValue = -1000;
            if (!batchExperience[i].crash) {
                qValue = snake.getQWithoutUpdate(batchExperience[i].s, batchExperience[i].a, batchExperience[i].s1, batchExperience[i].r)
            }
            let predict = nn.model.predict(tf.tensor([batchExperience[i].s])).dataSync()
            predict[batchExperience[i].a] = qValue;
            await nn.model.fit(tf.tensor([batchExperience[i].s]), tf.tensor([predict]), 10);
            dqn.replayMemory.clear()
        }
        snake = new Snake
    }
}

async function train_short_memory(s, a, s1, r) {
    let qValue = -1000;
    if (!snake.dead) {
        qValue = snake.getQWithoutUpdate(s, a, s1, r)
    }
    let predict = nn.model.predict(tf.tensor([s])).dataSync()
    predict[a] = qValue;
    await nn.model.fit(tf.tensor([s]), tf.tensor([predict]), 10);
}


function amax(x) {
    let xi = -1;
    let maxV = 0;
    for (let i =0;i < x.length;i++) {
        if (x[i] > maxV) {
            maxV = x[i]
            xi = i;
        }
    }
    return xi
}

function execAction(a) {
    if (MOVE_UP == a && snake.yspeed != -1) {
        snake.dir(0, 1);
    } else if (MOVE_DOWN == a && snake.yspeed != 1) {
        snake.dir(0, -1)
    } else if (MOVE_LEFT == a && snake.xspeed != 1) {
        snake.dir(-1, 0)
    } else if (MOVE_RIGHT == a && snake.xspeed != -1) {
        snake.dir(1, 0)
    }
    return false
}

function isItReverseAction(a) {
    if (MOVE_UP == a && snake.yspeed == -1) {
        return true;
    } else if (MOVE_DOWN == a && snake.yspeed == 1) {
        return true;
    } else if (MOVE_LEFT == a && snake.xspeed == 1) {
        return true
    } else if (MOVE_RIGHT == a && snake.xspeed == -1) {
        return true;
    }
    return false
}

function getXYForAction(a) {
    if (MOVE_UP == a) {
        return [0, 1]
    } else if (MOVE_DOWN == a) {
        return [0, -1]
    } else if (MOVE_LEFT == a) {
        return [-1, 0]
    } else if (MOVE_RIGHT == a && snake.xspeed == -1) {
        return [1, 0]
    }
}

function getCurrenctAction() {
    if (snake.xspeed == 0 && snake.yspeed == 1) {
        return 0;
    } else if (snake.xspeed == 0 && snake.yspeed == -1) {
        return 1;
    } else if (snake.xspeed == -1 && snake.yspeed == 0) {
        return 2;
    } else if (snake.xspeed == 1 && snake.yspeed == 0) {
        return 3;
    }
}
var scl = 20;
const W = 600;
const H = 600;

var snake, leftWall, topWall;

const FPS = 20;

var showBrain       = true;
var LEARNING_RATE   = 0.1;

const inputNodes    = 1;
const outputNodes   = 2;
const hiddenNodes   = 2;

var pause       = false;
var step        = false;
var userControl = true;
var check       = true;
var userActed   = false;

var pauseBtn, stepBtn, resumeBtn;
var LWM, TWM, RWM;
var LTCM;

function setup() {
    createCanvas(W, H);
    frameRate(FPS)

    LWM = new LW(undefined, {
        renderBrain : false,
    });

    TWM = new TW(undefined, {
        renderBrain : false,
    });

    RWM = new RW(undefined, {
        renderBrain : false,
    });

    BWM = new BW(undefined, {
        renderBrain : false,
    });

    LTCM = new LTC(undefined, {
        renderBrain : false
    })



    snake = new Snake();
    takeAction(2)

    this.setupButtons()
}   

function setupButtons() {
    pauseBtn = createButton('Pause')
    pauseBtn.position(700, 10)
    pauseBtn.mousePressed(pauseGame)

    stepBtn = createButton('Step ++')
    stepBtn.position(700, 30);
    stepBtn.mousePressed(stepGame)

    resumeBtn = createButton('Resume')
    resumeBtn.position(700, 60);
    resumeBtn.mousePressed(resumeGame)
}

function draw() {
    background(51)
    snake.show()

    let state = snake.getState();

    let isWallOnLeft    = state[0];
    let isWallOnTop     = state[1];
    let isWallOnRight   = state[2];
    let isWallAtBottom  = state[3];

    let willIDieOnLeft      = LWM.willIDieOnLeft(isWallOnLeft);
    let willIDieOnTop       = TWM.willIDieOnTop(isWallOnTop);
    let willIDieOnRight     = RWM.willIDieOnRight(isWallOnRight);
    let willIDieAtBottom    = BWM.willIDieAtBottom(isWallAtBottom);

    if (pause && !step ) return;

    let nnAction = undefined;

    let LTCState = [willIDieOnLeft, willIDieOnTop, snake.xspeed == -1 ? 1 : 0, snake.yspeed == -1? 1: 0]
    let LTCPredict = LTCM.predict(LTCState);
    LTCM.renderBrain(LTCState, LTCPredict)

    if (isInLeftTopCorner(state)) {
        let whereDoIFeelSafe = LTCM.whereDoIfeelSafe(LTCState)
        if (whereDoIFeelSafe != undefined) {
            nnAction = whereDoIFeelSafe;
        }
    } else {

        if (snake.xspeed == -1 && willIDieOnLeft == 1) {
            nnAction = 1;
        }

        if (snake.yspeed == -1 && willIDieOnTop == 1) {
            nnAction = 3;
        }

        if (snake.yspeed == 1 && willIDieAtBottom == 1) {
            nnAction = 2;
        }

        if (snake.xspeed == 1 && willIDieOnRight == 1){
            nnAction = 0;
        }
    }

    if (nnAction != undefined) {
        takeAction(nnAction)
    }



    snake.update();
    snake.death()


    if (snake.dead) {
        if (state[0] == 1 && snake.xspeed == -1)    LWM.memory.add([state[0]], [1, 0])
        if (state[1] == 1 && snake.yspeed == -1)    TWM.memory.add([state[1]], [1, 0])
        if (state[2] == 1 && snake.xspeed == 1)     RWM.memory.add([state[2]], [1, 0])
        if (state[3] == 1 && snake.yspeed == 1)     BWM.memory.add([state[3]], [1, 0])

        
        if (isInLeftTopCorner(state)) {
            let target = [0, 0, 0];
            if (nnAction == 2) {
                target = [1, 0, 0]
            }
            if (nnAction == 0) {
                target = [1, 0, 0]
            }
            if (nnAction == 1) {
                target = [0, 1, 0]
            }

            if (LTCPredict[1] > 0.9 && snake.yspeed == -1) {
                target = [1, 0, 0]
            }


            LTCM.badMemory.add(LTCState, target)



            LTCM.train();
        }

        LWM.train();
        TWM.train();
        RWM.train();
        BWM.train();

        snake = new Snake
        takeAction(2)

        // following will create new instance of modules and assign there brain and update current nueral network
        LWM = LWM.clone();
        TWM = TWM.clone();
        RWM = RWM.clone();
        BWM = BWM.clone();
        LTCM = LTCM.clone();

    } else {
       

        if (isInLeftTopCorner(state)) {
            let target = [0, 0, 0]
            if (LTCState[0] == 0 && LTCState[1] == 0 && LTCState[2] == 0 && LTCState[3] == 0) {
                target[2] = 1;
            } else {
                if (nnAction == 3) {
                    target[0] = 1;
                }else if (nnAction == 0) {
                    target[1]
                } else {
                    target[2] = 1;
                }
            }
            LTCM.goodMemory.add(LTCState, target)
        } else {
            LTCM.goodMemory.add(LTCState, [0, 0, 1])
            if (state[0] == 0 && snake.xspeed == -1)    LWM.memory.add([state[0]], [0, 1])
            if (state[1] == 0 && snake.yspeed == -1)    TWM.memory.add([state[1]], [0, 1])
            if (state[2] == 0 && snake.xspeed == 1)     RWM.memory.add([state[2]], [0, 1])
            if (state[3] == 0 && snake.yspeed == 1)     BWM.memory.add([state[3]], [0, 1])
        }
    }




    step        = false;
    userActed   = false;

    renderDots();
}

function isSnakeInCorner() {
    let s = snake.getState();
    return (s[0] == 1 && s[1] == 1) || (s[1] == 1 && s[2] == 1) || (s[2] == 1 && s[3] == 1) || (s[3] == 1 && s[0] == 1)
}

function isInLeftTopCorner(state) {
    return state[0] == 1 && state[1] == 1;
}

async function keyPressed() {
    if (keyCode == UP_ARROW && snake.yspeed != 1) {
        userActed = true
        snake.xspeed = 0;
        snake.yspeed = -1;
    } else if (keyCode == DOWN_ARROW && snake.yspeed != -1) {
        userActed = true
        snake.xspeed = 0;
        snake.yspeed = 1;
    } else if (keyCode == LEFT_ARROW && snake.xspeed != 1) {
        userActed = true
        snake.xspeed = -1;
        snake.yspeed = 0;
    } else if (keyCode == RIGHT_ARROW && snake.xspeed != -1) {
        userActed = true
        snake.xspeed = 1;
        snake.yspeed = 0;
    }
}

function takeAction(a) {
    switch (a) {
        case 0:
            snake.moveUp();
            break;
        case 1:
            snake.moveDown();
            break;
        case 2:
            snake.moveLeft()
            break;
        case 3:
            snake.moveRight()
            break;
    }
}

function resumeGame() {
    pause = false;
}

function pauseGame() {
    pause = true;
}

function stepGame() {
    step = true;
}

function renderDots() {
    renderDebugDots([[0, 220], [0, 240], [0, 260], [0, 280], [0, 300], [0, 320]],'left', LWM.nn, true)
    renderDebugDots([[220, 0], [240, 0], [260, 0], [280, 0], [300, 0], [320, 0]],'top', TWM.nn)
    renderDebugDots([[W-scl, 220], [W-scl, 240], [W-scl, 260], [W-scl, 280], [W-scl, 300], [W-scl, 320]],'right', RWM.nn, true)
    renderDebugDots([[220, H-scl], [240, H-scl], [260, H-scl], [280, H-scl], [300, H-scl], [320, H-scl]],'bottom', BWM.nn)
}

function renderDebugDots(cords, state, nn, vertical = false) {
    for (let i =0;i < cords.length;i++) {
        render2DotsInACell(cords[i][0], cords[i][1], state, nn, vertical);
    }
}